#   ******************************************
#   script for bind9 in the docker install
#   begin     : Thu 20 May 2021.
#   copyright : (c) 2022 Václav Dvorský
#   email     : vaclav.dvorsky@aricoma.com
#   $Id: init.sh, v1.80 21/06/2024
#   test on Ubuntu 22.04, Armbian 24.5.1 Jammy
#   ******************************************
#
#   --------------------------------------------------------------------
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public Licence as published by
#   the Free Software Foundation; either version 2 of the Licence, or
#   (at your option) any later version.
#   --------------------------------------------------------------------

#!/bin/bash
if ! [ $(id -u) = 0 ]; then
    if [ -d KLF ]
    then
        echo "The Bind9 directory already exists"
        exit 0
    fi

    # preparing the environment for the first run
    mkdir -p bind/config
    mkdir -p bind/cache
    mkdir -p bind/records
    mkdir -p bind/records/slave
    sudo chmod 640 bind/config/*
    sudo chmod 750 bind/*
    sudo chown -R 100:101 bind/*
    
  exit 0
fi
    echo "Do not run this script as root"
