$TTL 604800

@       IN    SOA  ns.example.com.   email.example.com. (
                              1         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800         ; Negative Cache TTL
);

            IN  NS      ns

ns          IN  A       192.168.88.2
underground IN  A       192.168.88.10
